<?php
/**
 * Created by PhpStorm.
 * User: Juris
 * Date: 31.10.2016.
 * Time: 4:16
 */
session_start();
class note {
    public $id,
        $title = '',
        $text = '',
        $color = 'default',
        $x = 0,
        $y = 0,
        $width,
        $height;
    function __construct() {
        $this->id = 0;
        $this->title = '';
        $this->text = '';
        $this->color = 'default';
        $this->x = 0;
        $this->y = 0;
    }
    function create() {
        if( isset( $_SESSION['notes'] ) ){
            $note_count = count($_SESSION['notes']);
            $last_id = end($_SESSION['notes']);
            $this->id = $last_id->id+1;
        }
        $_SESSION['notes'][$this->id] = $this;
    }
    function update() {
        $_SESSION['notes'][$this->id] = $this;
    }
    function delete($i) {
        unset($_SESSION['notes'][$i]);
    }
    function delete_all(){
        unset($_SESSION['notes']);
    }
    function view() {
        $data = json_encode($this);
        print_r($data);
    }
}

if(isset($_POST['item'])){
    $data = $_POST['item'];
    $note = new note();
    if($data['method'] == 'create'){
        $note->create();
        $note->view();
    } elseif ($data['method'] == 'update') {
        if(isset($data['id'])) $note->id = $data['id'];
        if(isset($data['title'])) $note->title = $data['title'];
        if(isset($data['text'])) $note->text = $data['text'];
        if(isset($data['width'])) $note->width = intval($data['width'])/$data['board_width'];
        if(isset($data['height'])) $note->height = intval($data['height'])/$data['board_height'];
        if(isset($data['x'])) $note->x = intval($data['x'])/$data['board_width'];
        if(isset($data['y'])) $note->y = intval($data['y'])/$data['board_height'];
        if(isset($data['color'])) $note->color = $data['color'];
        $note->update();
        $note->view();
    } elseif ($data['method'] == 'delete_all'){
        $note->delete_all();
    } elseif ($data['method'] == 'delete'){
        $note->delete($data['id']);
    }

}
if(isset($_GET['item'])){
    $data = json_encode($_SESSION['notes']);
    print_r($data);
}
?>
$( function () {
    // var board = $('.board');
    var boardWidth = $(window).width();
    var boardHeight= $(window).height() - $('header').height();
    var noteCount = parseInt($('.js-count').text());

    function updateNoteCount(count) {
        noteCount = count;
        $('.js-count').text(count);
    }

    function mkNote(data) {
        var el = '<div class="note ';
        if(data.color == 'red') el += 'note--red';
        el +='" data-id="'+data['id']+'">';
        el += '<div class="note__inner-wrapper">';
        el += '<div class="js-drag"></div>';
        el += '<div role="button" class="js-remove"></div>';
        el += '<div role="button" class="js-color"></div>';
        el += '<input class="note__title" name="title" placeholder="title" value="'+data.title+'">';
        el += '<textarea class="note__text" name="text" placeholder="text" style="width:'+ data.width*boardWidth+ ';height:' +data.height*boardHeight+';">'+data.text+'</textarea>';
        el += '</div></div>';
        $(el).appendTo('.board').css({
            'left': data.x*boardWidth,
            'top': data.y*boardHeight
        });
        $('.note').draggable({
            handler: '.js-drag',
            // containment: [0, $('header').height(), boardWidth, boardHeight]
        });
        updateNoteCount(noteCount+1);
        $('.note').on( "dragstop", function( event, ui ) {
            updateNote($(this));
        } );
    }

    function updateNote(note) {
        var c='default';
        if(note.hasClass('note--red')) c = 'red';
        $.post('note.php', {
            item: {
                method: 'update',
                id: note.attr('data-id'),
                title : note.find('.note__title').val(),
                text: note.find('.note__text').val(),
                x: note.css('left'),
                y: note.css('top'),
                width: note.find('.note__text').css('width'),
                height: note.find('.note__text').css('height'),
                board_width: boardWidth,
                board_height: boardHeight,
                color: c

            }
        });
    }
    $.get('note.php', {
        item: {
            method: 'init',
        }
    }, function (response) {
        $.each(response, function (i, note) {
            mkNote(note);
        })
    }, 'json');

    $('body').on('click', '.js-color', function (e) {
        $(e.target.offsetParent).toggleClass('note--red');
        updateNote($(e.target.offsetParent));
    });

    var resizeData = null;
    $('body').on('mousedown', '.note__text', function () {
        var w = $(this).width(),
            h = $(this).height();
        if($(this).css('width')) w = $(this).css('width');
        if($(this).css('height')) h = $(this).css('height');
        resizeData = {
            width: w,
            height: h
        }
    });
    $('body').on('mouseup', '.note__text', function (e) {
        var w = $(this).width(),
            h = $(this).height();
        if($(this).css('width')) w = $(this).css('width');
        if($(this).css('height')) h = $(this).css('height');
        if(resizeData.w - w !=0 || resizeData.h - h != 0) updateNote($(e.target.offsetParent))
        resizeData = null;
    });

    $('body').on('change', '.note__title', function (e) { updateNote($(e.target.offsetParent)) });
    $('body').on('change', '.note__text', function (e) { updateNote($(e.target.offsetParent)) });

    $('body').on('click','.js-add-note', function () {
        $.post('note.php', {
            item: {
                method: 'create'
            }
        }, function (response) {
            mkNote(response);
        }, 'json');
    });

    $('body').on('click', '.js-remove',function (e) {
        var target = $(e.target.offsetParent);
        $.post('note.php', {
            item: {
                method: 'delete',
                id: target.attr('data-id'),
            }
        }, function (response) {
            target.remove();
            updateNoteCount(noteCount-1);
        });
    });

    $('body').on('click', '.js-delete-all', function () {
        $.post('note.php', {
            item: {
                method: 'delete_all',
            }
        }, function (response) {
            $('.note').remove();
            updateNoteCount(noteCount = 0);
        });
    })
});
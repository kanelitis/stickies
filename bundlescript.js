/**
 * Created by Juris on 31.10.2016..
 */
var browserify = require('browserify'),
    fs = require('fs');

var b = browserify()
    .add('views/00_base/js/jquery.js')
    .add('views/00_base/js/jquery-ui.min.js')
    .add('views/03_organism/o_note/note.js')
    .bundle()
    .pipe(fs.createWriteStream('views/bundle.js'));
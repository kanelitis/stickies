<?php
    require_once 'vendor/autoload.php';

    $content = [];
    $content['settings'] = array(
      'views' => '/views'
    );

    $loader = new Twig_Loader_Filesystem('views');
    $twig = new Twig_Environment($loader, array(
        'cache' => false,
        'debug' => true,
    ));
    $twig->addExtension(new Twig_Extension_Debug());

    echo $twig->render('index.twig', $content);
?>


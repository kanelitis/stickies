# README #
how to install

* clone repo
* run: composer install
* setup virtualhost to roots folder and should be ready to go

how to edit js 

* edit js
* install node
* run: npm i browserify
* run: npm i fs
* edit bundlescript.js in root folder if needed
* run: node bundlescript.js in project root folder

features 

* can create, edit, delete notes (texts are saved on change so dont forget to change focus
* notes are saved in php session so they will stay some time
* can resize notes
* can change note to more importan by pressing warning sign on it 
* notes can be moved
* notes are trying to scale to window
* runs on MS edge, google chrom, mozilla firefox.

Techologies.

* SCSS
* JS -> jQuery(+ ui), browserify(for bundling js),
* HTML -> Twig (almost written in full BEM + Atom methodology)
* PHP 5.6